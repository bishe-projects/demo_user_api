package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

type Login struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (d *Login) ConvertToReq() *demo_user.UserLoginReq {
	return &demo_user.UserLoginReq{
		Username: d.Username,
		Password: d.Password,
	}
}
