package dto

import (
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

type Register struct {
	Username        string `json:"username" binding:"required"`
	Password        string `json:"password" binding:"required"`
	ConfirmPassword string `json:"confirm_password" binding:"required"`
}

func (d *Register) ConvertToReq() *demo_user.UserRegisterReq {
	return &demo_user.UserRegisterReq{
		Username:        d.Username,
		Password:        d.Password,
		ConfirmPassword: d.ConfirmPassword,
	}
}
