package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils/middleware"
	"gitlab.com/bishe-projects/demo_user_api/handler"
)

func main() {
	r := gin.Default()
	r.Use(middleware.Cors())
	user := r.Group("/user")
	{
		user.POST("/register", handler.Register)
		user.POST("/login", handler.Login)
		user.POST("/ownInfo", middleware.VerifyTokenMiddleware(), handler.OwnInfo)
		user.POST("/ownCommentList", middleware.VerifyTokenMiddleware(), handler.OwnCommentList)
	}
	r.Run("0.0.0.0:8086")
}
