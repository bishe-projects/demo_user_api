package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_user_api/client"
	"gitlab.com/bishe-projects/demo_user_api/dto"
	"net/http"
)

func Login(c *gin.Context) {
	var login dto.Login
	if err := c.BindJSON(&login); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	resp, err := client.UserClient.UserLogin(c, login.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
