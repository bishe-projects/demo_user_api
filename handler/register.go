package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/demo_user_api/client"
	"gitlab.com/bishe-projects/demo_user_api/dto"
	"net/http"
)

func Register(c *gin.Context) {
	var register dto.Register
	if err := c.BindJSON(&register); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	resp, err := client.UserClient.UserRegister(c, register.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
