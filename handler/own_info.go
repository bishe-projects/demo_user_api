package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/demo_user_api/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
	"net/http"
)

func OwnInfo(c *gin.Context) {
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	uid, bizErr := common_utils.GetUidFromCtx(ctx)
	if bizErr != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	req := &demo_user.GetUserByIDReq{Uid: uid}
	resp, err := client.UserClient.GetUserByID(ctx, req)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
