package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user/userservice"
)

var (
	UserClient = userservice.MustNewClient("demo_user_service", client.WithHostPorts("0.0.0.0:8889"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
